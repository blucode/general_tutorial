--in linux shell
$ sudo -i -u postgres
$ psql
--in psql
CREATE USER naomi WITH PASSWORD '******'; -- create database role with password
ALTER USER naomi WITH CREATEDB; --give 'naomi' priviledge to create dbs 

--as superuser, create initial db under naomi for access
CREATE DATABASE naomi;

--exit and reenter as naomi
\q
--login as user naomi
$ psql


/*SCHEMA
ONE TABLE FOR USR RECORDS: username (primary key), fname, lname, password, status
TABLE FOR LIST OF COURSES: coursename (primary key), number of students
TABLE FOR EACH USER: course (primary key), completion, comment
*/

--create database, schema nd tables as user naomi
CREATE DATABASE codeschool;
--no schema created, will use public schema
CREATE TABLE user_records(username text PRIMARY KEY, fname text, lname text,  password varchar(10) DEFAULT '0000', status text);
CREATE TABLE course_records(course_name text PRIMARY KEY, num_of_students integer DEFAULT 0);
CREATE TABLE user_template(course_name text PRIMARY KEY, completion numeric DEFAULT 0 CHECK (completion>=0), comments text);


/*write some function with queries*/

SELECT status FROM user_records WHERE username=X AND password=Y;
SELECT num_of_students FROM course_records WHERE course_name=X;
SELECT completion, comments FROM user_template WHERE course_name=X;


	 

